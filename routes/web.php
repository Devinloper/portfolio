<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['register' => false]);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('/panel', 'AdminController@index')->name('admin');
Route::get('/panel/user', 'UserController@index')->name('user');

Route::put('/panel/user/update', 'UserController@update')->name('user.update');

Route::get('/panel/contact', 'ContactController@index')->name('contact');

Route::get('/panel/about', 'AboutController@index')->name('about');
Route::put('/panel/about/update', 'AboutController@update')->name('about.update');
Route::put('/panel/about/image/update', 'AboutController@imageUpdate')->name('about.update.image');

Route::get('/panel/skills', 'SkillController@index')->name('skills');
Route::put('/panel/skills/{skill}/update', 'SkillController@update')->name('skills.update');
Route::put('/panel/skills/{skill}/image/update', 'SkillController@imageUpdate')->name('skills.update.image');
Route::put('/panel/skills/store', 'SkillController@store')->name('skills.store');
Route::delete('/panel/skills/{skill}/destroy', 'SkillController@destroy')->name('skills.destroy');

Route::get('/panel/projects', 'ProjectController@index')->name('projects')->middleware('auth');
Route::get('/panel/projects/{project}/edit', 'ProjectController@edit')->name('projects.edit')->middleware('auth');
Route::get('/panel/projects/{project}', 'ProjectController@edit')->name('projects.show');
Route::put('/panel/projects/{project}/update', 'ProjectController@update')->name('projects.update')->middleware('auth');
Route::put('/panel/projects/{project}/image/update', 'ProjectController@imageUpdate')->name('projects.update.image')->middleware('auth');
Route::put('/panel/projects/store', 'ProjectController@store')->name('projects.store')->middleware('auth');
Route::delete('/panel/projects/{project}/destroy', 'ProjectController@destroy')->name('projects.destroy')->middleware('auth');

Route::put('/panel/rows/create', 'RowController@store')->name('rows.store');
Route::delete('/panel/rows/{row}/delete', 'RowController@delete')->name('rows.delete');

Route::get('/panel/contact', 'ContactController@index')->name('contact')->middleware('auth');
Route::put('/panel/contact/store', 'ContactController@store')->name('contact.store');
Route::put('/panel/contact/{skill}/', 'ContactController@show')->name('contact.show')->middleware('auth');

Route::get('/panel/socials', 'SocialController@index')->name('socials');
Route::put('/panel/socials/update', 'SocialController@update')->name('socials.update');

