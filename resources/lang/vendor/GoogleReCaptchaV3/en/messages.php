<?php

return [
    'ERROR_MISSING_INPUT' => 'reCAPTCHA element could not be sent. Try again later.',
    'ERROR_UNABLE_TO_VERIFY' => 'Unable to verify reCAPTCHA. Try again later.',
    'ERROR_HOSTNAME' => 'Hostname does not match.',
    'ERROR_ACTION' => 'Action does not match.',
    'ERROR_SCORE_THRESHOLD' => 'Your current reCAPTCHA score does not meet the required threshold. Your message has been blocked.',
    'ERROR_TIMEOUT' => 'reCAPTCHA timeout. Try again later.',
    'SUCCESS' => 'Successfully passed.',
    'TIMEOUT_OR_DUPLICATE'=> 'reCAPTCHA timeout. Try again later.',
];
