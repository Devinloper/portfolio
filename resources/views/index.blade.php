<!doctype html>
<html lang="en">
<head>
    <title>My Portfolio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ $about->text }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

<div id="overlayer"></div>
<div class="loader">
    <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>


<div class="site-wrap">

    <div style="display: none; min-height: 100%;" class="container-fluid" id="selectableModal">
        <div class="row">
            <div class="col-md-6 col-12">
                <img class="img-fluid" id="modalImage" src="#" alt="#">
            </div>
            <div class="col-md-6 col-12">
                <h2 id="modalTitle" data-selectable="true">Ciao!</h2>
                <p id="modalDescription" data-selectable="true"></p>
                <a id="modalButton" href="#">
                    <button type="button" class="btn btn-info" data-selectable="true" href="#">Visit Project</button>
                </a>
            </div>
        </div>
    </div>

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>


    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

        <div class="container-fluid">
            <div class="row align-items-center">

                <div class="col-6 col-xl-2">
                    <h1 class="mb-0 site-logo"><a href="#home-section" class="h2 mb-0 smoothscroll">Devinloper<span
                                class="text-primary">.nl</span> </a></h1>
                </div>

                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right" role="navigation">

                        <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                            <li><a href="#home-section" class="nav-link">Home</a></li>
                            <li><a href="#about-section" class="nav-link">About Me</a></li>
                            <li><a href="#skills-section" class="nav-link">My Skills</a></li>
                            @isset($rows[0])<li><a href="#portfolio-section" class="nav-link">Projects</a></li>@endisset
                            <li><a href="#contact-section" class="nav-link">Contact</a></li>
                        </ul>
                    </nav>
                </div>


                <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a
                        href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a>
                </div>

            </div>
        </div>

    </header>


    <div class="site-blocks-cover overlay" style="background-image: url({{ asset('images/dreams.jpg') }});" data-aos="fade"
         id="home-section">

        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-8 mt-lg-5 text-center">
                    <h1 class="text-uppercase" data-aos="fade-up">Welcome</h1>
                    <p class="mb-5 desc" data-aos="fade-up" data-aos-delay="100">My portfolio Website</p>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <a href="#contact-section" class="btn smoothscroll btn-primary mr-2 mb-2">Get In Touch</a>
                    </div>
                </div>

            </div>
        </div>

        <a href="#about-section" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
        </a>
    </div>


    <div class="site-section cta-big-image" id="about-section">
        <div class="container">
            @isset($about)
                <div class="row mb-5">
                    <div class="col-12 text-center" data-aos="fade">
                        <h2 class="section-title mb-3">About Me</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 ml-5" data-aos="fade-up" data-aos-delay="100">
                        <div class="mb-4">
                            <h3 class="h3 mb-4 text-black">Who am I?</h3>
                            <p>{!! nl2br(e($about->text)) !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-5 mb-auto" data-aos="fade-up" data-aos-delay="">
                        <img src="{{ asset('storage/aboutMe/' . $about->image) }}" alt="{{ $about->image }}"
                             class="img-fluid col-12" width="75%" height="75%">
                    </div>
                </div>
            @endisset
        </div>
    </div>

    <section class="site-section border-bottom" id="skills-section">
        <div class="container">
            <div class="row mb-5 justify-content-center">
                <div class="col-md-8 text-center">
                    <h2 class="section-title mb-3" data-aos="fade-up" data-aos-delay="">My Skills</h2>
                </div>
            </div>
            <div class="row">
                @isset($skills)
                    @foreach($skills as $skill)
                        <div class="col-md-6 col-lg-3 mb-4" data-aos="fade-up" data-aos-delay="">
                            <div class="team-member">
                                <img src="{{ asset('storage/skills/' . $skill->image) }}" alt="{{ $skill->name }}"
                                     class="img-fluid">
                                <div class="p-3">
                                    <h3 style="word-wrap: break-word">{{ $skill->name }}</h3>
                                    <span class="position"
                                          style="word-wrap: break-word">{{ $skill->description }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>
        </div>
    </section>

    @isset($rows[0])
        <section class="site-section" id="portfolio-section">

            <div class="container">

                <div class="row mb-3">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h2 class="section-title mb-3">Portfolio</h2>
                    </div>
                </div>


                @foreach($rows as $row)
                    @switch($row->layout->id)
                        @case(1)
                            <div class="row text-center" data-aos="fade-up">
                                @isset($row->projects[0])
                                    <div class="item web col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 mb-4">
                                        <a onclick="fetch_project({{ $row->projects[0]->id }})" class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img class="img-fluid" style="width: 100%"
                                                 src="{{ asset('storage/projects/' . $row->projects[0]->image) }}"
                                                 alt="{{ $row->projects[0]->name }}">
                                        </a>
                                    </div>
                                @endisset

                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[1])
                                        <a onclick="fetch_project({{ $row->projects[1]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[1]->image) }}"
                                                 alt="{{ $row->projects[1]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[2])
                                        <a onclick="fetch_project({{ $row->projects[2]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[2]->image) }}"
                                                 alt="{{ $row->projects[2]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>

                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[3])
                                        <a onclick="fetch_project({{ $row->projects[3]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[3]->image) }}"
                                                 alt="{{ $row->projects[3]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[4])
                                        <a onclick="fetch_project({{ $row->projects[4]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[4]->image) }}"
                                                 alt="{{ $row->projects[4]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>
                            </div>
                            @break
                        @case(2)
                            <div class="row text-center" data-aos="fade-up">
                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[0])
                                        <a onclick="fetch_project({{ $row->projects[0]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[0]->image) }}"
                                                 alt="{{ $row->projects[0]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[1])
                                        <a onclick="fetch_project({{ $row->projects[1]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[1]->image) }}"
                                                 alt="{{ $row->projects[1]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>

                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[2])
                                        <a onclick="fetch_project({{ $row->projects[2]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[2]->image) }}"
                                                 alt="{{ $row->projects[2]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[3])
                                        <a onclick="fetch_project({{ $row->projects[3]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[3]->image) }}"
                                                 alt="{{ $row->projects[3]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>

                                @isset($row->projects[4])
                                    <div class="item web col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 mb-4">
                                        <a onclick="fetch_project({{ $row->projects[4]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[4]->image) }}"
                                                 alt="{{ $row->projects[4]->name  }}" class="img-fluid">
                                        </a>
                                    </div>
                                @endisset
                            </div>
                            @break
                        @case(3)
                            <div class="row text-center" data-aos="fade-up">
                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[0])
                                        <a onclick="fetch_project({{ $row->projects[0]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[0]->image) }}"
                                                 alt="{{ $row->projects[0]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[1])
                                        <a onclick="fetch_project({{ $row->projects[1]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[1]->image) }}"
                                                 alt="{{ $row->projects[1]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>

                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[2])
                                        <a onclick="fetch_project({{ $row->projects[2]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[2]->image) }}"
                                                 alt="{{ $row->projects[2]->name }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[3])
                                        <a onclick="fetch_project({{ $row->projects[3]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[3]->image) }}"
                                                 alt="{{ $row->projects[3]->name }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>

                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[4])
                                        <a onclick="fetch_project({{ $row->projects[4]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[4]->image) }}"
                                                 alt="{{ $row->projects[4]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[5])
                                        <a onclick="fetch_project({{ $row->projects[5]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[5]->image) }}"
                                                 alt="{{ $row->projects[5]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>

                                <div class="item web col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
                                    @isset($row->projects[6])
                                        <a onclick="fetch_project({{ $row->projects[6]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[6]->image) }}"
                                                 alt="{{ $row->projects[6]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                    <br>
                                    @isset($row->projects[7])
                                        <a onclick="fetch_project({{ $row->projects[7]->id }})"
                                           class="item-wrap">
                                            <span class="icon-search2"></span>
                                            <img src="{{ asset('storage/projects/' . $row->projects[7]->image) }}"
                                                 alt="{{ $row->projects[7]->name  }}" class="img-fluid"
                                                 style="max-height: 250px; max-width: 250px">
                                        </a>
                                    @endisset
                                </div>
                            </div>
                            @break
                    @endswitch
                @endforeach
            </div>
        </section>
    @endisset

    <section class="site-section bg-light" id="contact-section" data-aos="fade">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center">
                    <h2 class="section-title mb-3">Contact Me</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-5">
                    <form method="POST" action="{{ route('contact.store') }}" class="p-5 bg-white">
                        @csrf
                        @method('PUT')
                        <h2 class="h4 text-black mb-5">Contact Form</h2>

                        @if ($errors->any())
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        {!! implode('', $errors->all('<span>:message</span>')) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (Session::has('message'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        {{ session()->get('message') }}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-md-6 mb-3 mb-md-0">
                                <label class="text-black" for="fname">First Name</label>
                                <input type="text" id="fname" name="fname" class="form-control" required
                                       value="{{ old('fname') }}">
                            </div>
                            <div class="col-md-6">
                                <label class="text-black" for="lname">Last Name</label>
                                <input type="text" id="lname" name="lname" class="form-control" required
                                       value="{{ old('lname') }}">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="email">Email</label>
                                <input type="email" name="email" id="email" class="form-control" required
                                       value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="tel">Telephone Number (optional)</label>
                                <input type="tel" id="tel" name="tel" class="form-control" value="{{ old('tel') }}">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="message">Message</label>
                                <textarea maxlength="255" name="message" id="message" cols="30" rows="7"
                                          class="form-control"
                                          placeholder="Write your notes or questions here..."
                                          required>{{ old('message') }}</textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                {!! GoogleReCaptchaV3::renderField('contact_me_recaptcha', 'contact_me') !!}
                                <input type="submit" value="Send Message" class="btn btn-primary btn-md text-white">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                This site is protected by reCAPTCHA and the Google
                                <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                                <a href="https://policies.google.com/terms">Terms of Service</a> apply.
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 text-center">
                                @if($socials->facebook)
                                    <a href="{{ $socials->facebook }}" class="pr-3"><span
                                            class="icon-facebook h2 text-primary"></span></a>
                                @endif
                                @if($socials->twitter)
                                    <a href="{{ $socials->twitter }}" class="pr-3"><span
                                            class="icon-twitter h2 text-primary"></span></a>
                                @endif
                                @if($socials->instagram)
                                    <a href="{{ $socials->instagram }}" class="pr-3"><span
                                            class="icon-instagram h2 text-primary"></span></a>
                                @endif
                                @if($socials->linkedin)
                                    <a href="{{ $socials->linkedin }}" class="pr-3"><span
                                            class="icon-linkedin h2 te
                                        0...t-primary"></span></a>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>


    <footer class="site-footer">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="border-top pt-3">
                        <p>
                            Copyright &copy;<script async>document.write(new Date().getFullYear());</script>
                            All rights reserved
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div> <!-- .site-wrap -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/jquery.lockscroll.min.js"></script>

<script src="js/main.js"></script>

{!!  GoogleReCaptchaV3::init() !!}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function fetch_project(projectId) {
        $.ajax({
            url: "/panel/projects/" + projectId
        })
            .done(function (results) {
                $('#modalImage').attr('src', "{{ asset('/storage/projects/') }}/" + results.image);
                $('#modalImage').attr('alt', results.image);

                $('#modalTitle').text(results.name);
                $('#modalDescription').text(results.description);
                console.log(parseInt(results.toggle))
                if (parseInt(results.toggle)) {
                    console.log('reached')
                    $('#modalButton').attr('href', results.link);
                    $('#modalButton').show();
                } else {
                    $('#modalButton').attr('href', '#');
                    $('#modalButton').hide();
                }

                $.fancybox.open({
                    src: '#selectableModal',
                    type: 'inline',
                    touch: false,
                    opts: {
                        afterShow: function (instance, current) {
                            $.lockScroll(true);
                        },
                        afterClose: function (instance, current) {
                            $.lockScroll(false);
                        }
                    }
                });

                //$('#selectableModal').show();
            })
            .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                console.error("Error: " + errorThrown);
                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: "AJAX error " + textStatus + "! Please contact the application administrator."

                }, {
                    type: 'danger',
                    timer: 4000
                });
            });
    }
</script>

</body>

</html>
