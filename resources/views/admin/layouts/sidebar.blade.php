<div class="sidebar" data-color="purple" data-image="{{ asset('admin') }}/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text">
                Admin Panel
            </a>
        </div>

        <ul class="nav">
            <li class="{{ $activePage == 'about' ? ' active' : '' }}">
                <a href="{{ route('about') }}">
                    <i class="pe-7s-user"></i>
                    <p>About Me</p>
                </a>
            </li>
            <li class="{{ $activePage == 'skills' ? ' active' : '' }}">
                <a href="{{ route('skills') }}">
                    <i class="pe-7s-diamond"></i>
                    <p>Skills</p>
                </a>
            </li>
            <li class="{{ $activePage == 'projects' ? ' active' : '' }}">
                <a href="{{ route('projects') }}">
                    <i class="pe-7s-news-paper"></i>
                    <p>Projects</p>
                </a>
            </li>
            <li class="{{ $activePage == 'socials' ? ' active' : '' }}">
                <a href="{{ route('socials') }}">
                    <i class="pe-7s-way"></i>
                    <p>Social Media</p>
                </a>
            </li>
            <li class="{{ $activePage == 'table' ? ' active' : '' }}">
                <a href="{{ route('contact') }}">
                    <i class="pe-7s-note2"></i>
                    <p>Contact</p>
                </a>
            </li>
            <li class="{{ $activePage == 'user' ? ' active' : '' }}">
                <a href="{{ route('user') }}">
                    <i class="pe-7s-lock"></i>
                    <p>Change Password</p>
                </a>
            </li>
        </ul>
    </div>
</div>
