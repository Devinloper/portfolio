@extends('admin.layouts.app', ['activePage' => 'socials', 'titlePage' => 'Social Media'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Social Media Links</h4>
                        </div>
                        <div class="content">
                            <form method="POST" action="{{ route('socials.update') }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><ion-icon name="logo-twitter"></ion-icon></span>
                                                <input type="text" name="twitter" class="form-control" placeholder="https://twitter.com/[username]" value="{{ $socials->twitter ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><ion-icon name="logo-facebook"></ion-icon></span>
                                                <input type="text" name="facebook" class="form-control" placeholder="https://www.facebook.com/[username]" value="{{ $socials->facebook ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><ion-icon name="logo-instagram"></ion-icon></span>
                                                <input type="text" name="instagram" class="form-control" placeholder="http://instagram.com/[username]" value="{{ $socials->instagram ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><ion-icon name="logo-linkedin"></ion-icon></span>
                                                <input type="text" name="linkedin" class="form-control" placeholder="https://www.linkedin.com/in/[username]" value="{{ $socials->linkedin ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-info btn-fill pull-right" value="Send Update">
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
