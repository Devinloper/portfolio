@extends('admin.layouts.app', ['activePage' => 'skills', 'titlePage' => 'Skills'])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @isset($skills)
                    @foreach($skills as $skill)
                        <div class="col-md-3 showSkill" data-skillid="{{ $skill->id }}">
                            <div class="card">
                                <div class="content text-center">
                                    <img class="img-thumbnail" src="{{ asset('storage/skills/' . $skill->image) }}"
                                         alt="{{ $skill->image }}" width="250" height="250">
                                    <br>
                                    <h4 class="title">{{ $skill->name }}</h4>
                                    <button class="btn btn-info btn-fill editSkill" type="submit" data-skillId="{{ $skill->id }}">
                                            <span class="material-icons" style="font-size: 20px">edit</span>Edit Skill
                                    </button>
                                    <div class="clearfix"></div>
                                    <form method="POST" action="{{ route('skills.destroy', $skill) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-fill" type="submit"><span
                                                class="material-icons" style="font-size: 20px">delete_forever</span>Delete
                                            Skill
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 hidden updateSkillForm" data-skillid="{{ $skill->id }}">
                            <div class="card">
                                <div class="pull-left close closeUpdateSkill" style="color: darkred; font-size: 22px" data-skillid="{{ $skill->id }}"><i
                                        class="pe-7s-close-circle"></i></div>
                                <div class="header">
                                    <h4 class="title">Edit Skill</h4>
                                </div>
                                <div class="content text-center">
                                    <br>
                                    <form method="POST" action="{{ route('skills.update', $skill) }}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <img class="img-thumbnail" src="{{ asset('storage/skills/' . $skill->image) }}"
                                             alt="{{ $skill->image }}" width="250" height="250">
                                        <br>
                                        <input class="btn btn-info center-block" name="updateImage"
                                               type="file"
                                               value="{{ $skill->image }}">
                                        <br>
                                        <input class="form-control" name="updateName" type="text"
                                               placeholder="Name for new skill..." value="{{ $skill->name }}" required>
                                        <br>
                                        <textarea class="form-control" name="updateDescription"
                                                  type="text"
                                                  placeholder="Short description of skill..."
                                                  required>{{ $skill->description  }}</textarea>
                                        <br>
                                        <input class="btn btn-fill btn-info center-block" name="submitImage"
                                               type="submit"
                                               value="Save Changes">
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
                <div id="addNewSkillButton" class="col-md-3">
                    <div class="card">
                        <div class="content text-center">
                            <button class="btn btn-info btn-fill" id="newSkill">
                                <i class="pe-7s-plus" style="font-size: 22px"></i> Create New Skill
                            </button>
                        </div>
                    </div>
                </div>
                <div id="newSkillForm" class="col-md-3 hidden">
                    <div class="card">
                        <div id="closeNew" class="pull-left close" style="color: darkred; font-size: 22px"><i
                                class="pe-7s-close-circle"></i></div>
                        <div class="header">
                            <h4 class="title">Create New Skill</h4>
                        </div>
                        <div class="content">
                            <br>
                            <form method="POST" action="{{ route('skills.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input class="btn btn-info center-block" name="newImage" id="newImage" type="file"
                                       value="{{ old('newImage') }}" required>
                                <br>
                                <input class="form-control" name="newName" id="newName" type="text"
                                       placeholder="Name for new skill..." value="{{ old('newName') }}" required>
                                <br>
                                <textarea class="form-control" name="newDescription" id="newDescription" type="text"
                                          placeholder="Short description of skill..."
                                          required>{{ old('newDescription') }}</textarea>
                                <br>
                                <input class="btn btn-fill btn-info center-block" name="submitImage" type="submit"
                                       value="Save New Skill">
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#newSkill').on('click', function () {
            $('#newSkillForm').removeClass('hidden');
            $('#addNewSkillButton').addClass('hidden');
        });

        $('#closeNew').on('click', function () {
            $('#addNewSkillButton').removeClass('hidden');
            $('#newSkillForm').addClass('hidden');
            $('#newName').val(null);
            $('#newDescription').val(null);
            $('#newImage').val(null);
        });

        $('.editSkill').click(function () {
            let skillId = $(this).data('skillid');
            $('.updateSkillForm[data-skillid=' + skillId + ']').removeClass('hidden');
            $('.showSkill[data-skillid=' + skillId + ']').addClass('hidden');
        });

        $('.closeUpdateSkill').click(function () {
            let skillId = $(this).data('skillid');
            $('.updateSkillForm[data-skillid=' + skillId + ']').addClass('hidden');
            $('.showSkill[data-skillid=' + skillId + ']').removeClass('hidden');
        });
    </script>
@endpush
