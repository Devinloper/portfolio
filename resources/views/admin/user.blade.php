@extends('admin.layouts.app', ['activePage' => 'user', 'titlePage' => 'Change Password'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Profile</h4>
                        </div>
                        <div class="content">
                            <form method="POST" action="{{ route('user.update') }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Old Password</label>
                                                <input type="password" class="form-control" id="oldpass" name="oldpass" required value="{{ old('oldpass') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" class="form-control"
                                                   id="newpass" name="newpass" required value="{{ old('newpass') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Confirm New Password</label>
                                            <input type="password" class="form-control"
                                                   id="confirmpass" name="confirmpass" required value="{{ old('confirmpass') }}">
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-info btn-fill pull-right" value="Change Password">
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#oldpassview').click(function(){
            $(this).is(':checked') ? $('#oldpass').attr('type', 'text') : $('#oldpass').attr('type', 'password');
        });
    </script>
@endpush
