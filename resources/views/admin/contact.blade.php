@extends('admin.layouts.app', ['activePage' => 'table', 'titlePage' => 'Contact'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Contact Message List</h4>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Date</th>
                                <th>Message</th>
                                </thead>
                                <tbody>
                                @isset($contacts)
                                    @foreach($contacts as $contact)
                                        <tr>
                                            <td>{{ $contact->id }}</td>
                                            <td>{{ $contact->name }}</td>
                                            <td>{{ $contact->email }}</td>
                                            <td>{{ $contact->telephone }}</td>
                                            <td>{{ $contact->created_at }}</td>
                                            <td class="text-right" style="width: 50%"><a data-toggle="collapse" href="#collapseExample{{ $contact->id }}" role="button"
                                                   aria-expanded="false" aria-controls="collapseExample{{ $contact->id }}">
                                                    <i class="material-icons">keyboard_arrow_down</i></a>
                                                <br>
                                                <p id="collapseExample{{ $contact->id }}" class="collapse text-left">{!! nl2br(e($contact->message)) !!}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
