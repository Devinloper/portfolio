@extends('admin.layouts.app', ['activePage' => 'about', 'titlePage' => 'About Me'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit About Me</h4>
                        </div>
                        <div class="content">
                            <form method="POST" action="{{ route('about.update') }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="aboutMe" id="aboutMe" cols="30"
                                                      rows="20" required>{{ old('aboutMe') ?? $about->text }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <input class="btn btn-info btn-fill pull-right" type="submit" value="Update">
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Current Image</h4>
                        </div>
                        <div class="content">
                            <img class="center-block" src="{{ asset('storage/aboutMe/' . $about->image) }}"
                                 alt="about image" width="300">
                            <br>
                            <form method="POST" action="{{ route('about.update.image') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input class="btn btn-info center-block" name="image" id="image" type="file" required>
                                <br>
                                <input class="btn btn-fill btn-info center-block" name="submitImage" type="submit"
                                       value="Upload New Image">
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
