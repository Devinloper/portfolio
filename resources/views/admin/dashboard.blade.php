@extends('admin.layouts.app', ['activePage' => 'dashboard', 'titlePage' => 'Dashboard'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Welcome to your amazing Admin Panel!</h4>
                            <p class="category">Possibly the best Admin Panel ever made!</p>
                        </div>
                        <div class="content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
