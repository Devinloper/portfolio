@extends('admin.layouts.app', ['activePage' => 'projects', 'titlePage' => 'Projects'])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div id="newProjectForm" class="col-md-3 col-xs-12 hidden pull-right">
                    <div class="card">
                        <div id="closeNewProject" class="pull-left close" style="color: darkred; font-size: 22px"><i
                                class="pe-7s-close-circle"></i></div>
                        <div class="header">
                            <h4 class="title">Create New Project</h4>
                        </div>
                        <div class="content">
                            <br>
                            <form method="POST" action="{{ route('projects.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input class="btn btn-info center-block col-xs-12" style="margin-bottom: 10px" name="newImage" id="newImage" type="file"
                                       value="{{ old('newImage') }}" required>
                                <br>
                                <input class="form-control" name="newName" id="newName" type="text"
                                       placeholder="Name for new project..." value="{{ old('newName') }}" required>
                                <br>
                                <textarea class="form-control" name="newDescription" id="newDescription" type="text"
                                          placeholder="Short description of project..."
                                          required>{{ old('newDescription') }}</textarea>
                                <br>
                                <input type="text" class="form-control" id="newLink" name="newLink"
                                       placeholder="Link to project..." value="{{ old('newLink') }}">
                                <br>
                                <label for="newToggle" class="form-check-label">Visible Link?</label>
                                <input type="hidden" name="newToggle" value="0">
                                <input type="checkbox" class="form-check-input" id="newToggle" name="newToggle"
                                       value="1">
                                <br>
                                <label for="newRowId">Row</label>
                                <select name="newRowId" id="newRowId">
                                    @foreach($rows as $row)
                                        @if($newProjectAllowed[$row->id])
                                            <option value="{{ $row->id }}">Row {{ $row->id }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <input class="btn btn-fill btn-info center-block" name="submitImage" type="submit"
                                       value="Save New Project">
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12 hidden pull-right" id="updateProjectForm">
                    <div class="card">
                        <div class="pull-left close" id="closeUpdateProject"
                             style="color: darkred; font-size: 22px"><i
                                class="pe-7s-close-circle"></i></div>
                        <div class="header">
                            <h4 class="title">Edit Project</h4>
                        </div>
                        <div class="content text-center">
                            <br>
                            <form method="POST" id="updateForm"
                                  action="{{ route('projects.update', 1) }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <img class="img-thumbnail" id="updateThumbnail" style="margin-bottom: 10px" width="250" height="250">
                                <br>
                                <input class="btn btn-info center-block col-xs-12" style="margin-bottom: 10px"  name="updateImage" id="updateImage"
                                       type="file"
                                       value="">
                                <br>
                                <input class="form-control" name="updateName" type="text" id="updateName"
                                       placeholder="Name for new project..."
                                       value=""
                                       required>
                                <br>
                                <textarea class="form-control" name="updateDescription" id="updateDescription"
                                          type="text"
                                          placeholder="Short description of project..."
                                          required></textarea>
                                <br>
                                <input type="text" class="form-control" id="updateLink" name="updateLink"
                                       placeholder="Link to project..." value="{{ old('newLink') }}">
                                <br>
                                <label for="updateToggle" class="form-check-label">Visible Link?</label>
                                <input type="hidden" name="updateToggle" value="0">
                                <input type="checkbox" class="form-check-input" id="updateToggle" name="updateToggle"
                                       value="1">
                                <br>
                                <label for="updateRowId">Row</label>
                                <select name="updateRowId" id="updateRowId">
                                    @foreach($rows as $row)
                                        @if($newProjectAllowed[$row->id])
                                            <option value="{{ $row->id }}">Row {{ $row->id }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <input class="btn btn-fill btn-info center-block"
                                       name="submitUpdate"
                                       type="submit"
                                       value="Save Changes">
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                @isset($rows)
                    @foreach($rows as $row)
                        <div class="col-md-9 col-xs-12" style="border: 2px solid black">
                            <h4 class="col-md-9 col-xs-6">Row {{ $row->id }}</h4>
                            <br>
                            <form method="POST" action="{{ route('rows.delete', $row) }}" class="col-md-3 col-xs-6">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger col-xs-12"
                                        onclick="confirm('{{ __("Are you sure you want to delete this row? All projects inside will be deleted! You can\'t undo this action.") }}') ? this.parentElement.submit()  : ''">
                                    <span class="material-icons">delete_forever</span>Delete Row
                                </button>
                            </form>
                            <div class="row">
                            @isset($projects)
                                @foreach($projects as $project)
                                    @if($project->row_id === $row->id)
                                        <div class="col-md-4 col-xs-12 showProject" data-projectid="{{ $project->id }}">
                                            <div class="card">
                                                <div class="content text-center">
                                                    <img class="img-thumbnail"
                                                         src="{{ asset('storage/projects/' . $project->image) }}"
                                                         alt="{{ $project->image }}" width="250" height="250">
                                                    <br>
                                                    <h4 class="title">{{ $project->name }}</h4>
                                                    <button class="btn btn-info btn-fill editProject"
                                                            data-projectId="{{ $project->id }}"
                                                            onclick="fetch_project({{ $project->id }})">
                                                        <span class="material-icons" style="font-size: 20px">edit</span>Edit
                                                        Project
                                                    </button>
                                                    <div class="clearfix"></div>
                                                    <form method="POST"
                                                          action="{{ route('projects.destroy', $project) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-fill" type="submit"><span
                                                                class="material-icons" style="font-size: 20px">delete_forever</span>Delete
                                                            Project
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endisset
                            </div>
                            @if($newProjectAllowed[$row->id])
                                <div class="col-md" style="margin-bottom: 10px;">
                                    <button class="btn btn-info btn-fill newProject" value="{{ $row->id }}">
                                        <i class="pe-7s-plus" style="font-size: 22px"></i> Create New Project
                                    </button>
                                </div>
                            @endif
                        </div>
                    @endforeach
                @endisset
            </div>
        </div>
        <br>
        <div class="row">
            <div id="addNewRowButton" class="col-md-9">
                <div class="card">
                    <div class="content text-center">
                        <button class="btn btn-info btn-fill" id="newRow">
                            <i class="pe-7s-plus" style="font-size: 22px"></i> Create New Row
                        </button>
                    </div>
                </div>
            </div>
            <div id="newRowForm" class="col-md-9 hidden">
                <div class="card">
                    <div id="closeNewRow" class="pull-left close" style="color: darkred; font-size: 22px"><i
                            class="pe-7s-close-circle"></i></div>
                    <div class="header">
                        <h4 class="title">Create New Row</h4>
                    </div>
                    <div class="content">
                        <br>
                        <form class="text-center" method="POST" action="{{ route('rows.store') }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            @isset($layouts)
                                @foreach($layouts as $layout)
                                    <label class="btn">
                                        <button style="display: none" type="submit" name="layoutId"
                                                value="{{ $layout->id }}"></button>
                                        <img src="{{ asset('storage/layouts/layout' . $layout->id . '.png') }}" alt="{{ $layout->amount }}" height="125" width="250">
                                    </label>
                                @endforeach
                            @endisset
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('#newRow').on('click', function () {
            $('#newRowForm').removeClass('hidden');
            $('#addNewRowButton').addClass('hidden');
        });

        $('#closeNewRow').on('click', function () {
            $('#addNewRowButton').removeClass('hidden');
            $('#newRowForm').addClass('hidden');
        });

        $('#closeNewProject').on('click', function () {
            $('.newProject').removeClass('hidden');
            $('#newProjectForm').addClass('hidden');
            $('#newRowId *').removeAttr("selected");
            $('.newProject').attr("disabled", false);
            $('.editProject').attr("disabled", false);
        });

        $('.newProject').on('click', function () {
            $('#newProjectForm').removeClass('hidden');
            $('.newProject').attr("disabled", true);
            $('.editProject').attr("disabled", true);
            $('#newRowId').find('option[value=' + $(this).val() + ']').attr("selected", true);
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });

        $('#closeUpdateProject').on('click', function () {
            $('#updateName').val(null);
            $('#updateDescription').val(null);
            $('#updateLink').val(null);
            $('#updateToggle').attr('checked', false);
            $('#updateForm').attr('action', '');
            $('#updateRowId *').removeAttr("selected");
            $('#updateThumbnail').attr('src', "");
            $('#updateThumbnail').attr('alt', "");
            $('#updateProjectForm').addClass('hidden')
            $('.editProject').attr("disabled", false);
            $('.newProject').attr("disabled", false);
        })

        $('#updateToggle').on('click', function () {
            let attr = $(this).attr('clicked');
            if (typeof attr !== typeof undefined && attr !== false)
                $('#updateLink').attr('required', true);
            else
                $('#updateLink').attr('required', false);
        })

        function fetch_project(projectId) {
            $.ajax({
                url: "/panel/projects/" + projectId + "/edit"
            })
                .done(function (results) {
                    $('.editProject').attr("disabled", true);
                    $('.newProject').attr("disabled", true);

                    $('#updateName').val(results.name);
                    $('#updateDescription').val(results.description);
                    $('#updateLink').val(results.link);

                    if (results.toggle === 1)
                        $('#updateToggle').attr('checked', true);
                    else
                        $('#updateToggle').attr('checked', false);

                    if ($('#updateRowId').find('option[value=' + results.row_id + ']').length)
                        $('#updateRowId').find('option[value=' + results.row_id + ']').attr("selected", true);
                    else
                        $('#updateRowId').append('<option selected value=' + results.row_id + '>Row ' + results.row_id + '</option>');

                    $('#updateForm').attr('action', '/panel/projects/' + results.id + '/update');
                    $('#updateThumbnail').attr('src', "{{ asset('/storage/projects/') }}/" + results.image);
                    $('#updateThumbnail').attr('alt', results.image);

                    $('#updateProjectForm').removeClass('hidden')
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.error("Error: " + errorThrown);
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: "AJAX error " + textStatus + "! Please contact the application administrator."

                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                });
        }
    </script>
@endpush
