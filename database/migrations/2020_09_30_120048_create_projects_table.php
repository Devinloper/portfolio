<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->char("name");
            $table->text("description");
            $table->char("image");
            $table->char("link")->nullable();
            $table->boolean("toggle");
            $table->bigInteger("row_id")->unsigned();
            $table->timestamps();

            $table->foreign("row_id")->references("id")->on("rows")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
