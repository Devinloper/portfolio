<?php

use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->insert([
            'text' => "Lorum Ipsum",
            'image' => "default-avatar.png",
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
