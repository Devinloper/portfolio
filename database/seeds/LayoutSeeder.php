<?php

use Illuminate\Database\Seeder;

class LayoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('layouts')->insert([
            'amount' => 5,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('layouts')->insert([
            'amount' => 5,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('layouts')->insert([
            'amount' => 8,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
