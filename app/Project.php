<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name', 'description', 'image', 'link', 'toggle', 'row_id'
    ];

    public function row()
    {
        return $this->hasOne('App\Row');
    }
}
