<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Row extends Model
{
    protected $fillable = [
        'layout_id'
    ];

    public function layout()
    {
        return $this->belongsTo('App\Layout');
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}
