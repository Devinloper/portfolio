<?php

namespace App\Http\Controllers;

use App\Row;
use Illuminate\Http\Request;

class RowController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Save the row information to the database.
     * */
    public function store(Request $request)
    {

        Row::create([
            'layout_id' => $request->layoutId,
        ]);

        return redirect()->back()->with('message', 'New Row Created Successfully');
    }

    /*
     * Delete the row information from the database.
     * */
    public function delete(Row $row)
    {
        try {
            $row->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Row Deleted Successfully');
    }
}
