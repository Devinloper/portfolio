<?php

namespace App\Http\Controllers;

use App\Layout;
use App\Project;
use App\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    /*
     * Returns all project, layout and row information to the panel. Also checks if there are any more projects allowed in the layout.
     * */
    public function index()
    {
        $projects = Project::all();
        $layouts = Layout::all();
        $rows = Row::all();
        $newProjectAllowed = [];

        foreach ($rows as $row) {
            if ($row->loadCount('projects')->projects_count >= $row->layout->amount) {
                $newProjectAllowed[$row->id] = false;
            } else {
                $newProjectAllowed[$row->id] = true;
            }
        }

        return view('admin.projects', compact(['projects', 'layouts', 'rows', 'newProjectAllowed']));
    }

    /*
     * Saves project information to the database.
     * */
    public function store(Request $request)
    {
        $this->validateInput();
        $image = $this->validateImage();

        Project::create([
            'name' => $request->newName,
            'description' => $request->newDescription,
            'image' => $image,
            'link' => $request->newLink,
            'toggle' => $request->newToggle,
            'row_id' => $request->newRowId,
        ]);

        return redirect()->back()->with('message', 'New Project Created Successfully');
    }

    /*
     * This function is called by Ajax for getting the inforation of a project to edit. It is also called to get
     * individial information on a project on the main page.
     * */
    public function edit(Project $project)
    {
        return Project::find($project->id);
    }

    /*
     * Update the project data in the database.
     * */
    public function update(Project $project, Request $request)
    {
        $image = $project->image;

        if (isset($request->updateImage)) {
            $image = $this->validateImage($project, false);
        }

        $this->validateInput(true);

        $project->update([
            'name' => $request->updateName,
            'description' => $request->updateDescription,
            'image' => $image,
            'link' => $request->updateLink,
            'toggle' => $request->updateToggle,
            'row_id' => $request->updateRowId
        ]);

        return redirect()->back()->with('message', 'Project updated successfully');
    }

    /*
     * Delete project data from the database and delete the project image.
     * */
    public function destroy(Project $project)
    {
        try {
            if (file_exists('storage/projects/' . $project->image)) {
                File::delete('storage/projects/' . $project->image);
            }
            $project->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Project deleted successfully');
    }

    /*
     * Validate the text input of the creation project form and the edit project form
     *
     * If $update is false, the validation is treated as a new project. If it is false, it is treated as an update.
     * */
    public function validateInput(bool $update = false)
    {
        if (!$update)
            $rules = [
                'newName' => ['required', 'max:255'],
                'newDescription' => ['required', 'max:255'],
                'newLink' => ['max:255'],
                'newToggle' => ['max:255']
            ];
        else
            $rules = [
                'updateName' => ['required', 'max:255'],
                'updateDescription' => ['required', 'max:255'],
                'updateLink' => ['max:255'],
                'updateToggle' => ['max:255']
            ];

        Validator::make(request()->all(), $rules)->validate();
    }

    /*
     * Validate the uploaded image and save it.
     *
     * If $required is false, the validation is treated as a new project. If it is true, it is treated as an update.
     * */
    public function validateImage(Project $project = null, bool $required = true)
    {
        if ($required) {
            $rules = [
                'newImage' => ['required', 'mimes:jpeg,jpg,png,bmp', 'dimensions:min_width=250,min_height=250']
            ];
        } else {
            $rules = [
                'newImage' => ['mimes:jpeg,jpg,png,bmp', 'dimensions:min_width=250,min_height=250']
            ];
        }

        $messages = [
            'newImage.dimensions' => 'The minimal dimensions for a skill image are 250x250'
        ];

        Validator::make(request()->all(), $rules, $messages)->validate();

        // Resizes the image to 500x500px
        if ($required)
            $file = Image::make(request()->file('newImage'))->resize(500, 500);
        else
            $file = Image::make(request()->file('updateImage'))->resize(500, 500);

        $destinationPath = 'storage/projects/';

        if ($required) {
            $nextId = DB::select("SHOW TABLE STATUS LIKE 'projects'")[0]->Auto_increment;
            $imageName = 'projectImage_' . $nextId;
        } else {
            $imageName = 'projectImage_' . $project->id;
        }

        // If the project directory does not exist, create it.
        if (!Storage::disk('public')->exists('projects/'))
            Storage::disk('public')->makeDirectory('projects/');

        if ($required)
            $extension = request()->file('newImage')->getClientOriginalExtension();
        else
            $extension = request()->file('updateImage')->getClientOriginalExtension();

        $fileName = $imageName . '.' . $extension;

        if (Storage::disk('public')->exists($destinationPath . '/' . $imageName)) {
            Storage::disk('public')->delete($destinationPath . '/' . $imageName);
        }

        // Save the image.
        $file->save(public_path($destinationPath . $fileName));

        return $fileName;
    }
}
