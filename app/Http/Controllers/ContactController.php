<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;

class ContactController extends Controller
{
    /*
     * Return all the contact messages to the panel.
     * */
    public function index()
    {
        $contacts = Contact::all();
        return view('admin.contact', compact(['contacts']));
    }

    /*
     * This saves the new contact message to the database. If the validator fails it will return to the previous page with errors.
     * */
    public function store(Request $request)
    {
        $validator = $this->validateInput();

        if ($validator)
        {
            return redirect()->to(URL::previous() . "#contact-section")->withErrors($validator)->withInput();
        }

        Contact::create([
            'name' => $request->fname . ' ' . $request->lname,
            'email' => $request->email,
            'telephone' => $request->tel,
            'message' => $request->message
        ]);

        return redirect()->to(URL::previous() . "#contact-section")->with('message', 'Message sent successfully');
    }

    /*
     * Validates the contact form input and returns the errors if the validator fails.
     * */
    public function validateInput()
    {
        $rules = [
            'fname' => ['required', 'max:255'],
            'lname' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'tel' => ['max:255'],
            'message' => ['required', 'max:255'],
            'g-recaptcha-response' => [new GoogleReCaptchaV3ValidationRule('contact_me')]
        ];

        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors()->messages();
        }
    }
}
