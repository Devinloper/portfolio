<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Return the About Me information to the view.
     * */
    public function index()
    {
        $about = About::find(1);
        return view('admin.about', compact(['about']));
    }

    /*
     * Update the database with the new About Me text.
     * */
    public function update(Request $request)
    {
        $this->validateInput();

        $aboutText = About::find(1)->update(['text' => $request->aboutMe]);

        return redirect()->back()->with('message', 'Text updated successfully');
    }

    /*
     * Validate the text input.
     * */
    public function validateInput()
    {
        $rules = [
            'aboutMe' => ['required']
        ];

        Validator::make(request()->all(), $rules)->validate();
    }

    /*
     * Saves the uploaded image and updates the image name in the database.
     * */
    public function imageUpdate(Request $request)
    {
        $validated = $this->validateImage();

        $validated['file']->storeAs($validated['destinationPath'], $validated['fileName'], 'public');

        About::find(1)->update(['image' => $validated['fileName']]);

        return redirect()->back()->with('message', 'Successfully updated image!');
    }

    /*
     * Validates the image input and prepares it for storing.
     * */
    public function validateImage()
    {
        $rules = [
            'image'=>'required|mimes:jpeg,jpg,png,bmp',
        ];

        Validator::make(request()->all(), $rules)->validate();

        $file = request()->file('image');

        $destinationPath = 'aboutMe/';
        $imageName = 'aboutMeImage';

        // Check if an image with the set name already exists, if so, delete it.
        if (Storage::disk('public')->exists($destinationPath . '/' . $imageName))
        {
            Storage::disk('public')->delete($destinationPath . '/' . $imageName);
        }

        $extension = $file->getClientOriginalExtension();
        $fileName = $imageName.'.'.$extension;

        return ['file' => $file, 'fileName' => $fileName, 'destinationPath' => $destinationPath];
    }
}
