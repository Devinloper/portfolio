<?php

namespace App\Http\Controllers;

use App\About;
use App\Skill;
use App\Social;
use App\Row;

class HomeController extends Controller
{
    /*
     * Get all data that needs to be displayed on the main page.
     * */
    public function index()
    {
        $socials = Social::find(1);
        $skills = Skill::all();
        $about = About::find(1);
        $rows = Row::all();
        return view('index', compact(['socials', 'skills', 'about', 'rows']));
    }
}
