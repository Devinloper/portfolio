<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.user');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function update(Request $request)
    {
        // If the entered new password is not the same as the password of the current logged in user, return an error.
        if (!Hash::check($request->oldpass, Auth::user()->getAuthPassword()))
            return redirect()->route('user')->withErrors("Old password is incorrect.");

        $this->validateInput();

        // Update the password in the database.
        User::find(Auth::id())->update(['password' => Hash::make($request->newpass)]);

        return redirect()->back()->with('message', 'Password change successful');
    }

    /*
     * Validate the text input.
     * */
    public function validateInput()
    {
        $rules = [
          'oldpass' => ['required'],
          'newpass' => ['required'],
          'confirmpass' => ['required', 'same:newpass']
        ];

        $messages = [
          'confirmpass.same' => 'The new passwords don\'t match'
        ];

        Validator::make(request()->all(), $rules, $messages)->validate();
    }
}
