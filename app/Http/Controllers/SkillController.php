<?php

namespace App\Http\Controllers;

use App\Skill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SkillController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Returns all Skills to the panel to display.
     * */
    public function index()
    {
        $skills = Skill::all();
        return view('admin.skills', compact(['skills']));
    }

    /*
     * Saves new skill data to the database.
     * */
    public function store(Request $request)
    {
        $this->validateInput();
        $image = $this->validateImage();

        Skill::create([
            'name' => $request->newName,
            'description' => $request->newDescription,
            'image' => $image,
        ]);

        return redirect()->back()->with('message', 'New Skill Created Successfully');
    }

    /*
     * Delete the skill image and delete the skill data from the database.
     * */
    public function destroy(Skill $skill)
    {
        try {
            if (file_exists('storage/skills/' . $skill->image)) {
                File::delete('storage/skills/' . $skill->image);
            }
            $skill->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Skill deleted successfully');
    }

    /*
     * Update the skill inforamtion in the database.
     * */
    public function update(Request $request, Skill $skill)
    {
        $image = $skill->image;

        if (isset($request->updateImage)) {
            $image = $this->validateImage($skill,false);
        }

        $this->validateInput(true);

        $skill->update([
            'name' => $request->updateName,
            'description' => $request->updateDescription,
            'image' => $image
        ]);

        return redirect()->back()->with('message', 'Skill updated successfully');
    }

    /*
     * Validate the text input for the skill
     *
     * If $update is false, the validation is treated as a new skill. If it is true, it is treated as an update.
     * */
    public function validateInput(bool $update = false)
    {
        if (!$update)
            $rules = [
                'newName' => ['required', 'max:255'],
                'newDescription' => ['required', 'max:255']
            ];
        else
            $rules = [
                'updateName' => ['required', 'max:255'],
                'updateDescription' => ['required', 'max:255']
            ];

        Validator::make(request()->all(), $rules)->validate();
    }

    /*
     * Validate the uploaded image and save it.
     *
     * If $required is false, the validation is treated as a new skill. If it is true, it is treated as an update.
     * */
    public function validateImage(Skill $skill = null, bool $required = true)
    {
        if ($required) {
            $rules = [
                'newImage' => ['required', 'mimes:jpeg,jpg,png,bmp', 'dimensions:min_width=250,min_height=250']
            ];
        } else {
            $rules = [
                'newImage' => ['mimes:jpeg,jpg,png,bmp', 'dimensions:min_width=250,min_height=250']
            ];
        }

        $messages = [
            'newImage.dimensions' => 'The minimal dimensions for a skill image are 250x250'
        ];

        Validator::make(request()->all(), $rules, $messages)->validate();

        // Resizes the image to 500x500px.
        if ($required)
            $file = Image::make(request()->file('newImage'))->resize(500, 500);
        else
            $file = Image::make(request()->file('updateImage'))->resize(500, 500);

        $destinationPath = 'storage/skills/';

        if ($required) {
            $nextId = DB::select("SHOW TABLE STATUS LIKE 'skills'")[0]->Auto_increment;
            $imageName = 'skillImage_' . $nextId;
        } else {
            $imageName = 'skillImage_' . $skill->id;
        }

        // If the skills directory does not exist, create it.
        if (!Storage::disk('public')->exists('skills/'))
            Storage::disk('public')->makeDirectory('skills/');

        if ($required)
            $extension = request()->file('newImage')->getClientOriginalExtension();
        else
            $extension = request()->file('updateImage')->getClientOriginalExtension();

        $fileName = $imageName . '.' . $extension;

        if (Storage::disk('public')->exists($destinationPath . '/' . $imageName)) {
            Storage::disk('public')->delete($destinationPath . '/' . $imageName);
        }

        // Save the image.
        $file->save(public_path($destinationPath . $fileName));

        return $fileName;
    }
}
